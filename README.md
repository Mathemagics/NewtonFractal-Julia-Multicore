# Purpose
 
 This is a newton fractal generator written in Julia. 
 It runs on 8 processor cores.
 
 It's purpose was to:
 1. Learn and explore Julia (a very early version)
 2. Learn and explore parallel programming.
 3. Learn and explore distributed programming.

 
 
# Results:
 
This Julia version looks a bit messy, I didn't get much performance increase from parallelization. I would do more tests, but I don't see much benefit from it.
The python version of this program performs the slowest, but was single core only.
The haskell version is much easier to reason, and is much faster than either of them, at the cost of ram.
 
 
 
## Times for Julia

8 Cores, Resolution:300

2.708862 seconds (1.65 M allocations: 72.351 MB, 1.25% gc time)

SingleCore, Resolution:300

2.959584 seconds (24.17 M allocations: 774.900 MB, 3.97% gc time)

## Times for Haskell REPA with Lazy Evaluation
8 cores, Resolution 300 

~2 seconds (Need to perform benchmark)
 
