addprocs(7)
using Gadfly

@everywhere function julia(maxim,epsilon,z)
    f(x)=z^5-z^3+z-1.0
    fp(x)=5z^4-3z^2+1.0
    #f(x)=sin(x)
    #fp(x)=cos(x)

    for i in range(1,maxim)
        oldZ=z
        z = oldZ-(f(oldZ)/fp(oldZ))
        #println(abs(z-oldZ))
        if abs(z-oldZ)<=epsilon && i>3 ;return i;break;end

        if i==maxim return i; break; end
    end
end


function delta(minimum,maximum,resolution)
    return (maximum-minimum)/float(resolution)
end

function juliaSet(minimum,maximum, resolution, epsilon, maxim)
    return imgCycle(minimum, maximum,resolution,epsilon,maxim)
end

function initVec(resolution)
  image=SharedMatrix
  @parallel for i in resolution
    for j in resolution
      image[i,j]=1
      end
      end
  return image
  end

function imgCycle(minimum, maximum,resolution,epsilon,maxim)
    image=SharedArray(Int,resolution,resolution)
    dx = delta(minimum,maximum,resolution)
    dy = dx

      @sync @parallel for ix in range(1,resolution)
       for iy in range(1,resolution)
            z=(minimum+dx*ix)+(1im)*(minimum+dy*iy)
            image[iy,ix]=julia(maxim,epsilon,z)
        end
        #print("Cycle: " , ix , "of ", resolution)
    end
    return image
end

function hippiesSpiralingIntoGrooviness()
    epsilon=.0001
    maxim=50
    resolution=30
    minimum=-10
    maximum=10
    hippies=juliaSet(minimum,maximum,resolution,epsilon,maxim)
    spy(hippies)
end


hippiesSpiralingIntoGrooviness()


#Times
#8 Cores Resolution:300
#2.708862 seconds (1.65 M allocations: 72.351 MB, 1.25% gc time)

#SingleCore Resolution:300
#2.959584 seconds (24.17 M allocations: 774.900 MB, 3.97% gc time)
